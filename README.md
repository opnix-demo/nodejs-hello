# NodeJS sample program

## Project Introduction

This project is a simple one [NodeJS](http://nodejs.org) example，from the official website. directory Structure：

```
.
├── Procfile
├── README.md
├── app.js
└── package.json
```

## Project requirements

### Package manager

NodeJS program needs to use [npm](https://www.npmjs.org) package manager，and must `package.json` file must exist，if not, please use `npm init` The command creates and configures the required dependencies and other information, otherwise the application will not be deployed. `package.json` please refer to the specific configuration items. [npm document](https://www.npmjs.org/doc/json.html)。

`package.json` file example：

```
{
  "name": "default-nodejs-app",
  "version": "0.0.1",
  "author": "Your Name",
  "dependencies": {
    "express": "3.4.8",
    "consolidate": "0.10.0",
    "express": "3.4.8",
    "swig": "1.3.2",
  },
  "engines": {
    "node": "0.10.x",
    "npm": "1.3.x"
  }
}
```

### Listening port

NodeJS application needs to use `PORT` from the environment, the port that service needs to listen to. example：

```
app.listen(process.env.PORT || 5000;
```

### Run command

should be in `Procfile` specify the application's startup command in the file, example：

```
web: node app.js
```



## local test

1. Execute `npm install` to install the required dependencies.
2. Start the web server by executing `node app.js`.
3. Visit <http://localhost:5000> to preview the result
